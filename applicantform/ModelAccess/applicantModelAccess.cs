﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelView;
using ModelData;

namespace ModelAccess
{
    public class applicantModelAccess
    {
        public static string Message;
        public static List<applicantModelView> GetListAll()
        {
            List<applicantModelView> listData = new List<applicantModelView>();

            using (var db = new applicant_dbEntities())
            {
                listData = (from a in db.tb_applicant
                            where a.is_delete == false
                            select new applicantModelView
                            {
                                id = a.id,
                                is_delete = a.is_delete,
                                nama = a.nama,
                                nomor_hp = a.nomor_hp,
                                email = a.email,
                                tgl_lahir = a.tgl_lahir,
                                alamat = a.alamat,
                                jurusan = a.jurusan,
                                pertanyaan = a.pertanyaan

                            }).ToList();
            }
            return listData;
        }

        public static bool Insert(applicantModelView paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new applicant_dbEntities())
                {
                    tb_applicant a = new tb_applicant();
                    a.is_delete = paramModel.is_delete;
                    a.nama = paramModel.nama;
                    a.nomor_hp = paramModel.nomor_hp;
                    a.email = paramModel.email;
                    a.tgl_lahir = paramModel.tgl_lahir;
                    a.alamat = paramModel.alamat;
                    a.jurusan = paramModel.jurusan;
                    a.pertanyaan = paramModel.pertanyaan;

                    db.tb_applicant.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        //Mengedit data berdasarkan primary key menggunakan parameter
        public static applicantModelView GetDetailsById(int paramId)
        {
            //penampung datanya
            applicantModelView result = new applicantModelView();

            using (var db = new applicant_dbEntities())
            {
                result = (from a in db.tb_applicant
                          where a.id == paramId
                          select new applicantModelView
                          {
                              id = a.id,
                              is_delete = a.is_delete,
                              nama = a.nama,
                              nomor_hp = a.nomor_hp,
                              email = a.email,
                              tgl_lahir = a.tgl_lahir,
                              alamat = a.alamat,
                              jurusan = a.jurusan,
                              pertanyaan = a.pertanyaan
                          }).FirstOrDefault();
            }
            return result;
        }

        //Update data
        public static bool Update(applicantModelView paramModel)
        {
            //penampung return
            bool result = true;

            try
            {
                using (var db = new applicant_dbEntities())
                {
                    //Mengambil data lama
                    tb_applicant a = db.tb_applicant.Where(o => o.id == paramModel.id).FirstOrDefault();

                    if (a != null)
                    {
                        a.is_delete = paramModel.is_delete;
                        a.nama = paramModel.nama;
                        a.nomor_hp = paramModel.nomor_hp;
                        a.email = paramModel.email;
                        a.tgl_lahir = paramModel.tgl_lahir;
                        a.alamat = paramModel.alamat;
                        a.jurusan = paramModel.jurusan;
                        a.pertanyaan = paramModel.pertanyaan;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static bool Delete(applicantModelView paramModelDelete)
        {
            //variabel penampung status
            bool result = true;

            try
            {
                using (var db = new applicant_dbEntities())
                {
                    //Memanggil tabel
                    tb_applicant a = db.tb_applicant.Where(o => o.id == paramModelDelete.id).FirstOrDefault();
                    if (a != null)
                    {
                        a.is_delete = true;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }
    }
}
