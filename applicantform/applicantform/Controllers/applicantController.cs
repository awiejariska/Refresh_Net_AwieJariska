﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelView;
using ModelAccess;
using applicantform.Models.Shared;

namespace applicantform.Controllers
{
    public class applicantController : Controller
    {
        // GET: applicant
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            List<applicantModelView> List = new List<applicantModelView>();
            List = applicantModelAccess.GetListAll();
            return PartialView("List", List);
        }

        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(applicantModelView paramModel)
        {
            try
            {
                paramModel.is_delete = false;

                if (ModelState.IsValid)
                {
                    return Json(new
                    {
                        success = applicantModelAccess.Insert(paramModel),
                        message = applicantModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<Message> listMessage =
                     Message.GetMessageErrorByModel("paramModel", ModelState);

                    return Json(new
                    {
                        success = false,
                        message = listMessage
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(Int32 paramId)
        {
            return PartialView(applicantModelAccess.GetDetailsById(paramId));
        }

        [HttpPost]
        public ActionResult Edit(applicantModelView paramModel)
        {
            try
            {

                return Json(new
                {
                    success = applicantModelAccess.Update(paramModel),
                    message = applicantModelAccess.Message
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                },
                JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Delete(Int32 IdDelete)
        {
            return PartialView(applicantModelAccess.GetDetailsById(IdDelete));
        }

        [HttpPost]
        public ActionResult Delete(applicantModelView paramModelDelete)
        {
            try
            {
                paramModelDelete.is_delete = true;

                return Json(new
                {
                    success = applicantModelAccess.Delete(paramModelDelete),
                    message = applicantModelAccess.Message
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}