﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelView
{
    public class applicantModelView
    {
        public long id { get; set; }
        public bool is_delete { get; set; }
        public string nama { get; set; }
        public string nomor_hp { get; set; }
        public string email { get; set; }
        public System.DateTime tgl_lahir { get; set; }
        public string alamat { get; set; }
        public string jurusan { get; set; }
        public string pertanyaan { get; set; }
    }
}
