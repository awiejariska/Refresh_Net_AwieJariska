USE [applicant_db]
GO
/****** Object:  Table [dbo].[tb_applicant]    Script Date: 7/8/2020 3:57:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_applicant](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[is_delete] [bit] NOT NULL,
	[nama] [varchar](50) NOT NULL,
	[nomor_hp] [varchar](13) NULL,
	[email] [varchar](50) NULL,
	[tgl_lahir] [date] NOT NULL,
	[alamat] [varchar](100) NULL,
	[jurusan] [varchar](50) NULL,
	[pertanyaan] [varchar](150) NULL,
 CONSTRAINT [PK_tb_applicant] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
